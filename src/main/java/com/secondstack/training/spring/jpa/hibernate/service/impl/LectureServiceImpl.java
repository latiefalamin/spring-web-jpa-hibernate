package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.Lecture;
import com.secondstack.training.spring.jpa.hibernate.domain.Subject;
import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;
import com.secondstack.training.spring.jpa.hibernate.domain.enumeration.Semester;
import com.secondstack.training.spring.jpa.hibernate.service.LectureService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class LectureServiceImpl implements LectureService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Lecture lecture) {
        entityManager.persist(lecture);
    }

    @Override
    public Lecture update(Integer id, Lecture lecture) {
        Lecture oldLecture = findById(id);
        oldLecture.setSemester(lecture.getSemester());
        oldLecture.setSubject(lecture.getSubject());
        oldLecture.setTeacher(lecture.getTeacher());
        oldLecture.setYear(lecture.getYear());
        return entityManager.merge(oldLecture);
    }

    @Override
    public void delete(Lecture lecture) {
        entityManager.remove(lecture);
    }

    @Override
    public void delete(Integer id) {
        entityManager.remove(findById(id));
    }

    @Override
    public List<Lecture> findAll() {
        List<Lecture> results = entityManager.createQuery("SELECT o from Lecture o", Lecture.class).getResultList();
        return results;
    }

    @Override
    public Lecture findById(Integer id) {
        return entityManager.find(Lecture.class, id);
    }

    @Override
    public Lecture findBySubjectAndTeacherAndSemesterAndYear(Subject subject, Teacher teacher, Semester semester, Integer year) {
        return entityManager.createQuery(
                "SELECT o FROM Lecture o " +
                        "where o.subject = :subject AND o.teacher = :teacher AND o.semester = :semester AND o.year = :year"
                , Lecture.class)
                .setParameter("subject", subject).setParameter("teacher", teacher)
                .setParameter("semester", semester).setParameter("year", year)
                .getSingleResult();
    }
}
