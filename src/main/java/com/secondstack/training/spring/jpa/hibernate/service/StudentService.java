package com.secondstack.training.spring.jpa.hibernate.service;

import com.secondstack.training.spring.jpa.hibernate.domain.Student;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface StudentService {
    void save(Student student);
    Student update(Integer id, Student student);
    void delete(Student student);
    void delete(Integer id);
    List<Student> findAll();
    Student findById(Integer id);
}
