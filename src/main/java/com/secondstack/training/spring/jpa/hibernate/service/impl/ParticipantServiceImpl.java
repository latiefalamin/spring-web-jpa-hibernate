package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.Participant;
import com.secondstack.training.spring.jpa.hibernate.service.ParticipantService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class ParticipantServiceImpl implements ParticipantService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Participant participant) {
        entityManager.persist(participant);
    }

    @Override
    public Participant update(Integer id, Participant participant){
        Participant oldParticipant = findById(id);
        oldParticipant.setLecture(participant.getLecture());
        oldParticipant.setStudent(participant.getStudent());
        oldParticipant.setValue(participant.getValue());
        return entityManager.merge(oldParticipant);
    }

    @Override
    public void delete(Participant participant){
        entityManager.remove(participant);
    }

    @Override
    public void delete(Integer id){
        entityManager.remove(findById(id));
    }

    @Override
    public List<Participant> findAll(){
        List<Participant> results = entityManager.createQuery("SELECT s from Participant s", Participant.class).getResultList();
        return results;
    }
    @Override
    public Participant findById(Integer id){
        return entityManager.find(Participant.class, id);
    }
}
